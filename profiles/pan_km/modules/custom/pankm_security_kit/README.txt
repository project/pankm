INTRODUCTION
-------------
* You can turn the features and functionality on by installing the module,
  and you can turn it off by uninstalling the module.

INSTALLATION
------------
 
* Install as you would normally install a contributed Drupal module. 
   Visit https://www.drupal.org/docs/user_guide/en/extend-module-install.html 
   for further information.

PURPOSE
---------
* Purpose of the module is to remove or alter attachments on the page, 
  or add attachments to the page that depend on another module's attachments.

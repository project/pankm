<?php

namespace Drupal\pankm_security_kit\Controller;

/**
 * @file
 * Contains \Drupal\hello_world\HelloWorldController.
 */


/**
 * Provides route responses for the hello world page example.
 */
class SecurityController {

  /**
   * Returns a simple hello world page.
   *
   * @return array
   *   A very simple renderable array is returned.
   */
  public function myCallbackMethod() {
    $element = [
      '#markup' => '<p><b>Saying Hello World in Drupal 8 is cool!</b></p>',
    ];
    return $element;
  }

}

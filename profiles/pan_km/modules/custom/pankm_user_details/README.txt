USER DETAILS MODULE
-------------------

This is a  custom module to create a custom field for user entity.
While enabling this module, following fields will be created.
1. First Name
2. Last Name 	
3. Gender 	
4. Address 	
5. Biography 	
6. Description 	
7. Facebook 	
8. Job Title 	
9. Linked In 	
10.Organization 	
11.Phone 	
12.Skype Id 	
13.Twitter 	
14.Website

By default these field will be hidden in the form display
(User add and edit form).To enable it goto 
admin/config/people/accounts/form-display and then 
enable above fields and arrange it.
